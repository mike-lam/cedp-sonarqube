FROM sonarqube
RUN wget https://github.com/prometheus/node_exporter/releases/download/v1.1.2/node_exporter-1.1.2.amd64.tar.gz
RUN tar xvfz node_exporter-1.1.2.amd64.tar.gz
WORDIR node_exporter-1.1.2.amd64
?? ./node_exporter

